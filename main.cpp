#define uint32_t unsigned long

int main(int argc, char** argv)
{
    uint32_t peb = 0;
    char *base;
    const char *module = "KERNEL32.DLL";
    const char *fname1 = "GetStdHandle";
    uint32_t func1;
    const char *fname2 = "WriteConsoleA";
    uint32_t func2;
    uint32_t *curr;
    uint32_t *end;
    uint32_t i;
    int written;

#ifdef __GNUC__
    __asm__ __volatile__(
        "movl %%fs:0x30, %0"
        :"=r"(peb)
        );
#else
    __asm
    {
        mov ebx, fs:[0x30]
        mov peb, ebx
    }
#endif
    curr = (uint32_t*)(*(uint32_t*)((*(uint32_t*)(peb + 12)) + 0x4*5));
    end = (uint32_t*)(*(uint32_t*)((*(uint32_t*)(peb + 12)) + 0x4*6));
    do
    {
        char temp = 0;
        uint32_t i = 0;
        do
        {
            temp = ((char*)*(curr + 10))[i*2];
            if(temp >= 'a' && temp <= 'z')
                temp -= ' ';

            if(temp != module[i])
                goto next;

            if(!module[i])
                break;
            i++;
        }while(temp != 0);
        base = *(char**)(curr + 0x4);
        break;
next:
        curr = (uint32_t*)*curr;
    }while(end != curr);

    if(!base)
        return 1;

#define GetProc(_name, _len, _func)\
    if(len == _len)\
    {\
    for(j = 0; j < _len; j++)\
    if(name[j] != _name[j])\
    goto errf1;\
    _func = (uint32_t)base + ((uint32_t*)(base + *(uint32_t*)((*(uint32_t*)(base + *((uint32_t*) base + 0xF) + 0x78) + base) + 0x1C)))[((short*)(base + *(uint32_t*)((*(uint32_t*)(base + *((uint32_t*) base + 0xF) + 0x78) + base) + 0x24)))[i]];\
    }

    for(i = 0; i < *(uint32_t*)((*(uint32_t*)(base + *((uint32_t*) base + 0xF) + 0x78) + base) + 0x18); i++)
    {
        char *name = ((char**)(base + *(uint32_t*)((*(uint32_t*)(base + *((uint32_t*) base + 0xF) + 0x78) + base) + 0x20)))[i] + (uint32_t)base;
        uint32_t len;
        uint32_t j;
        for(len = 0; len < 0xFFFF; len++)
            if(name[len] == 0)
                break;
        GetProc(fname1, 12, func1);
        GetProc(fname2, 13, func2);
errf1:        
        continue;
    }
#undef GetProc
    ((int (__stdcall*)(void*, const void *, int, int*, void*))func2)(((void* (__stdcall*)(int))func1)(-11), "Hello World!", 12, &written, 0);

    return 0;
}
